import { Component, OnInit } from '@angular/core';
import {Personne} from '../../model/Personne';

@Component({
  selector: 'app-cv',
  templateUrl: './cv.component.html',
  styleUrls: ['./cv.component.css']
})
export class CvComponent implements OnInit {

  personnes: Personne[] ;
  selectedPersonne: Personne ;
  constructor() { }

  ngOnInit(): void {
    this.personnes = [
      new Personne(1, 'ndiaye', 'moussa', 30, 'moussa.jpg', 43944, 'Engineer'),
      new Personne(2, 'ndiaye', 'moussa', 30, 'rotating_card_thumb.png', 43944, 'Engineer'),
      new Personne(3, 'diop', 'abdoulaye', 32, 'rotating_card_profile.png', 43944, 'Full stack developer')

    ];
  }


  selectedPersonList(selectedListCV): void {
    this.selectedPersonne = selectedListCV;;
  }
}
