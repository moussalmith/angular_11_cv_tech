import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Personne} from '../../model/Personne';

@Component({
  selector: 'app-item-cv',
  templateUrl: './item-cv.component.html',
  styleUrls: ['./item-cv.component.css']
})
export class ItemCvComponent implements OnInit {

  @Input() itemPersonne: Personne;
  @Output() selectedPersonne = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

  selectPersonne(): void {
    this.selectedPersonne.emit(this.itemPersonne);
  }
}
