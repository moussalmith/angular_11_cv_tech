import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Personne} from '../../model/Personne';

@Component({
  selector: 'app-liste-cv',
  templateUrl: './liste-cv.component.html',
  styleUrls: ['./liste-cv.component.css']
})
export class ListeCvComponent implements OnInit {

  @Input() personnesListe: Personne[];
  @Output() selectedPersonList = new EventEmitter<Personne>();
  constructor() { }

  ngOnInit(): void {
  }

  selectPersonne(selectedPersonItem): void {
    this.selectedPersonList.emit(selectedPersonItem);
  }
}
